# Elive-Quick-Keymap-changer
### the next release will have an option for switching 'on-the-fly' using a pre-defined key combo (alt+shift as default) to switch the keymap .... and a systray icon doing the same.

A small utility in 'yad' that quickly changes the existing keymap and variant to the wanted on.
~~This is does not offer a systray option or a key-combo like 'Alt + Shift' to make fixed changes.~~

That can be done in the future but will make it a tad less small. :)

<img src="2023-05-17_23-05.png" alt="2023-05-17_23-05" style="zoom:44%;" />

<img src="2023-05-15_12-52.png" alt="2023-05-15_12-52" style="zoom:44%;" />



<img src="2023-05-15_12-56.png" style="zoom:44%;" />
