#! /bin/bash

SOURCE="$0"
source /usr/lib/elive-tools/functions
EL_REPORTS="1"
el_make_environment normal
#. /usr/bin/gettext.sh
#TEXTDOMAIN="elive-quick-keymap-changer"

MODEL_CHOICE=$(setxkbmap -query | grep rules |awk -F'      ' '{print $2}')

RULE_CHOICE=$(setxkbmap -query | grep model |awk -F'      ' '{print $2}')

CURRENT=$(setxkbmap -query | grep layout |awk -F'    ' '{print $2}')


LAYOUT=`sed '/^! layout$/,/^ *$/!d;//d' "/usr/share/X11/xkb/rules/base.lst" | sort -u| awk -F' '  '{print $1"..."$2".."$3}'`


Language=$(yad --borders=5 --center --image=logo-elive --title="Quick Keymap Changer" --no-headers --text="<b>Choose your Layout Language...</b>\nCurrent Map: <b>$CURRENT</b>" --image-on-top --height=500 --list --search-column=1 --column="<b>Language-Layout</b>":$LAYOUT)

LANG_CHOICE=`echo $Language |awk -F'\.' '{print $1}'`

FLAG="/usr/share/icons/Enlightenment-X/intl/128/flag-$LANG_CHOICE.png"

Variant=$(yad --borders=5 --center --title="Quick Keymap Changer" --image=$FLAG --no-headers --text="<b>Choose your Kbd Variant...\n Or click Cancel to use the standard layout.</b>" --geometry=500x300 --list --search-column=1 --column="<b>Variant-Layout</b>":`sed '/^! variant$/,/^ *$/!d;//d' "/usr/share/X11/xkb/rules/base.lst" | grep $LANG_CHOICE: | awk -F' ' '{print $1}'`)
# Not using 'sort -u' here, as it surpresses i.e 'altgr-intl' if 'alt-intl' is also there.

VARIANT_CHOICE=`echo $Variant |awk -F'|' '{print $1}'`

yad --center --borders=5 --image=$FLAG --title="Confirm" --text="you entered: <b>- $LANG_CHOICE</b> and <b>- $VARIANT_CHOICE</b>\n Do you want to set this as your keymap?" --button="Abort:0" --button="Change:1"
ret=$?
[[ $ret -eq 0 ]] && exit
[[ $ret -eq 1 ]] && /bin/sh -c "setxkbmap -rules $RULE_CHOICE -model $MODEL_CHOICE -layout $LANG_CHOICE -variant $VARIANT_CHOICE"
